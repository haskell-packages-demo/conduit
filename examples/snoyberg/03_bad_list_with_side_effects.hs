import Conduit
    ( (.|)
    , mapC
    , mapM_C
    , runConduit
    , takeC
    , takeWhileC
    , yieldMany
    )

import Data.Foldable
    (mapM_)

import Prelude
    ( ($), (<), (*)
    , Integer
    , IO
    , map
    , print
    , putStrLn
    , take
    , takeWhile
    )

-----

complicatedList :: IO ()
complicatedList = mapM_ print
    $ takeWhile (< 18) $ map (* 2) $ take 10 [(1 :: Integer)..]

complicatedConduit :: IO ()
complicatedConduit = runConduit
     $ yieldMany [(1 :: Integer)..]
    .| takeC 10
    .| mapC (* 2)
    .| takeWhileC (< 18)
    .| mapM_C print

main :: IO ()
main = do
    putStrLn "List version:"
    complicatedList
    putStrLn ""
    putStrLn "Conduit version:"
    complicatedConduit
