import Conduit
    ( (.|)
    , mapC
    , runConduitPure
    , sinkList
    , takeC
    , takeWhileC
    , yieldMany
    )

import Prelude
    ( ($), (<), (*)
    , Integer
    , IO
    , map
    , print
    , putStrLn
    , take
    , takeWhile
    )

-----

complicatedList :: IO ()
complicatedList = print
    $ takeWhile (< 18) $ map (* 2) $ take 10 [(1 :: Integer)..]

complicatedConduit :: IO ()
complicatedConduit = print $ runConduitPure
     $ yieldMany [(1 :: Integer)..]
    .| takeC 10
    .| mapC (* 2)
    .| takeWhileC (< 18)
    .| sinkList

main :: IO ()
main = do
    putStrLn "List version:"
    complicatedList
    putStrLn ""
    putStrLn "Conduit version:"
    complicatedConduit
