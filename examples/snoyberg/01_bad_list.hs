import Conduit
    ( (.|)
    , runConduitPure
    , sinkList
    , takeC
    , yieldMany
    )

import Prelude
    ( ($)
    , Integer
    , IO
    , print
    , putStrLn
    , take
    )

-----

take10List :: IO ()
take10List = print
    $ take 10 [(1 :: Integer)..]

take10Conduit :: IO ()
take10Conduit = print $ runConduitPure
    $ yieldMany [(1 :: Integer)..] .| takeC 10 .| sinkList

main :: IO ()
main = do
    putStrLn "List version:"
    take10List
    putStrLn ""
    putStrLn "Conduit version:"
    take10Conduit
