import Conduit
    ( (.|)
    , mapM_C
    , mapMC
    , runConduit
    , takeC
    , takeWhileC
    , yieldMany
    )

import Data.Foldable
    (mapM_)

import Prelude
    ( ($), (<), (*), (.), (++), (>>=)
    , Integer
    , IO
    , mapM
    , print
    , putStrLn
    , return
    , show
    , take
    , takeWhile
    )

-----

magic :: Integer -> IO Integer
magic x = do
    putStrLn $ "I'm doing magic with " ++ show x
    return $ x * 2

magicalList :: IO ()
magicalList =
    mapM magic (take 10 [(1 :: Integer)..]) >>= mapM_ print . takeWhile (< 18)

magicalConduit :: IO ()
magicalConduit = runConduit
     $ yieldMany [(1 :: Integer)..]
    .| takeC 10
    .| mapMC magic
    .| takeWhileC (< 18)
    .| mapM_C print

main :: IO ()
main = do
    putStrLn "List version:"
    magicalList
    putStrLn ""
    putStrLn "Conduit version:"
    magicalConduit
