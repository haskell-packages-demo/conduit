# conduit

Streaming data processing library. https://www.stackage.org/package/conduit

* There is a derivative included in Foundation
* [haskell conduit tutorial](
  https://google.com/search?q=haskell+conduit+tutorial)
* *Practical Haskell*
  2019 Alejandro Serrano Mena
  * Ch. 9. Dealing with Files: IO and Conduit
* [*Web Programming and Streaming Data in Haskell*
  ](https://www.snoyman.com/reveal/conduit-yesod#/)
  2017 Michael Snoyman
* *Haskell Design Patterns*
  2015 Ryan Lemmer
  * Iteratee I/O
